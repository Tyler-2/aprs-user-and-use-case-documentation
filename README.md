# APRS User and Use-Case Documentation

## Bulletins and Announcements

These functions are meant to populate a several line "bulletin board" that is consistent across an area. So if you want to know "what's going on" in an area, you could just pull up the announcement board. Unfortunately most APRS software/hardware doesn't implement the receive-end of this very well. Also, some radio displays can display fewer characters than others. But let's talk about how it SHOULD work!

*As with most APRS features, senders of packets need to take special care to use APRS features appropriately and carefully, as one misbehaving sender can cause
long-term devalutation of a given feature for a very wide area. Additionally, digipeater operators should be ready to disable digipeating of packets from problematic senders. The below information is not exhaustive for best practices when sending these packets - that is a longer discussion, and one worth having if you're interested in sending these packet types!*

**RF spectrum is a limited, shared resource. The APRS network is an even more limited resource. The bulletin boards are even MORE limited. Be considerate of what/how often you transmit, use sensible PATHs, clean up old messages, don't blow away other peoples' bulletin board objects.**

### Announcements
![Announcements display example](./images/Announcement.png)
(Page 83 of APRS spec)

Messages that may be transmitted for a few or several days, but not frequently. Perhaps every hour. These are perfect for announcing an upcoming meeting / tail-gate. If you have a regularly scheduled weekly net, it probably only makes sense to transmit this a day or two ahead of the net; I think Net schedules should typically be on the repeater object they're associated with though, if possible. But if there's an up-coming annual tail-gate, it would make sense to announce that for longer. This is not a place to advertise your website, club, a frequency you're listening on, etc. These must be of wide applicability to amateur radio operators and *should* have some timeliness. APRS receivers are probably not configured to notify of new/changed announcements.

### General Bulletins
![General Bulletin display example](./images/General Bulletin.png)
General Bulletins (Page 83 of APRS spec)

Time sensitive messages sent to everyone in the area. These will usually be transmitted for a few hours at most, a few times per hour. As mentioned in the documentations, general bulletins are time sensitive (which implies urgency), so might include weather warnings (though there is another message type specifically for this), widespread power outages, etc. These are DEFINITELY not a place for spam. Ideally APRS receivers would pop up a notification when there is a change detected to a line on the general bulletin board.

### Group Bulletins
![Group Bulletin display example](./images/Group Bulletin.png)

Group Bulletins (Page 84 of APRS spec): The previous two message types are sent to everyone, so senders have to be very careful and considerate about what they bother folks with. But what if you have a Mongolian Rug Dying group that you want to maintain announcements for? Group Bulletins! Group Bulletins can be associated with a club, a net, your neighborhood, whatever. Messages sent to a group bulletin board will naturally go out on all the typical APRS RF paths (so keep it sane!), but only receivers configured to notify/display a specific group's messages will display them. On the receiving end, you should be able to view or subscribe to groups that are specifically of interest to you.

### Putting it all together
A simplex net that meets weekly might publish an **ANNOUNCEMENT** of their net, along with their group identifier, such as "SPLX4". Then they could post whatever they wanted to the SPLX4 group bulletin board, and it would only be targeting people specifically interested in the SPLX4 net. Very consideraate!

### Bad implementations, improper use
Page 76 of the [APRS spec](http://www.aprs.org/doc/APRS101.PDF) covers a lot of how APRS bulletins are meant to be implemented.

https://aprs.fi/bulletin shows all bulletin messages sent from within 500km of your location. This is a good way to troubleshoot bulletins, but a poor way to consume them. All bulletin message types, whether announcements, group related, etc are lumped into one table. It's best to think of each type of bulletin board as a display, and each entry (A-Z, 0-9) as a line on that display. APRS.FI doesn't really get that across very well.

Xastir shows bulletins as popup messages but I haven't received enough of them to get a feel for how good the implementation is.

APRSDroid sucks at this.

There is a station "near" me transmitting what I would call a malformed bulletin packet (no ID, so it can't be classified as a general, group, announcement, etc), that is really just advertising the location of a satellite igate. I don't think this is a proper use of the bulletin board system, as it's not timely, it's a use-case covered by their existing igate beacon, and the system would collapse/not work if everyone used it to broadcast services they were running. I have reached out via email but not gotten a response back. Such is life in Amateur Radio, I suppose!

### Further information

There are other types of message types, such as NWS BULLETINS and NTS RADIOGRAMS which might be covered in a future update.

### How do I send these?

Future documentation specifically for transmitters will cover this.
